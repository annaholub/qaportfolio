# QAportfolio



## What a repository is it

This repository is a collection of test documentation developed during the Prometheus Internship program for QA manual in partnership with GlobalLogic. The Internship program corresponds to the process of working with the web portal and mobile application from testing the customer's requirements to post-test learning. 


The repository is replenished as the internship progresses.


## A few words about the Skybot project

Skybot is a company selling and servicing individual electric vehicles and scooters for rent.

The project aims to develop a web portal and mobile app for Skybot.

Main business processes to provide: individual electric vehicle sales, test drive registration, and technical service registration.

Additional functions: reviews, video-hosting integration, support chat, reminders, and members' group.


## Who am I

My name is Anna Holub, I am a trainee QA engineer. In 2023 I completed the QA Manual course from Prometheus in partnership with GlobalLogic. Since November 2023 I have been a student of the QA manual Internship program from Prometheus in partnership with GlobalLogic. 

**My LinkedIn page**: https://www.linkedin.com/in/annaholubeditor/.
